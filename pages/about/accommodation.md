---
name: Accommodation
---

# Accommodation

Attendees with an accommodation [bursary](/about/bursaries) will be accommodated
at the [Hotel Four Points by Sheraton Kochi Infopark][four-points].

[four-points]: https://www.marriott.com/en-us/hotels/cokfp-four-points-by-sheraton-kochi-infopark/overview/

Most of the conference venue and hacklabs will be at the hotel itself.

<a class="btn btn-info" role="button"
href="https://www.openstreetmap.org/node/9658370139">
  <i class="fa fa-map" aria-hidden="true"></i> Map
</a>

The rooms will be shared between 2 people. See these
[pictures](https://www.marriott.com/en-us/hotels/cokfp-four-points-by-sheraton-kochi-infopark/photos/)
of the facilities.

Hotel wifi will be available for all the guests in their rooms and other public
areas inside the hotel. There will be DebConf wifi at all the venues/hacklabs.


## Hotels
If you are looking for a hotel near to Infopark, here is a list:

**4/5 Star Hotels**

- [Hotel Four Points by Sheraton Kochi Infopark][four-points]. Self-paying attendees can book rooms directly with the hotel. Contact [the DebConf registration team](mailto:registration@debconf.org) for a discount code. Rooms booked through this code include 3 meals a day.
- [Novotel Kochi Infopark](https://all.accor.com/hotel/B283/index.en.shtml)
- [Hotel Olive Eva](https://www.olivehotels.com/eva.php)

We’d suggest checking booking aggregator websites for better offers.

**2/3 Star Hotels**

- [Lavender Business hotel Infopark](https://lavenderkochi.com/)
- [The Blooms Cochin](https://bloomscochin.in/)

Support small businesses. Book directly.
