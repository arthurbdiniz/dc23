---
name: The Conference Venue
---

# Venue
DebConf 23 will be held in venues in two buildings, 300 metres apart.

## Infopark - Athulya Hall
Athulya Hall inside the Infopark campus will be the main hall for the DebConf.

## Four Points Hotel
The Four Points Hotel where we have the accomodation will also have other halls and hacklabs.

- Cinnamon 1
- Cinnamon 2
- Sage
- Board Room
- Businness Lounge
- All Spice

# Getting to Kochi
Cochin (Nedumbassery) International Airport, Cochin (IATA: COK, ICAO: VOCI) is
an international airport serving the city of Kochi, in the state of Kerala,
India. Located at Nedumbassery, about 25 kilometres (16 mi) northeast of the
city, Cochin International Airport is the busiest and largest airport in the
state of Kerala. It is also the fourth busiest airport in India in terms of
international traffic and eighth busiest overall.

[Kochi airport airlines and destinations](https://en.wikipedia.org/wiki/Cochin_International_Airport#Airlines_and_destinations)
# Getting to venue
## From Cochin International Airport (CIAL)
### Pre-paid Taxi
Pre-paid taxi would be the easiest and best way to reach the venue.
Infopark/Four Points Hotel is about 28 km from the the airport. The pre-paid
taxi would cost you around Rs.1000-1200 (12-15 USD).

If you plan to book a pre-paid taxi, **you should pre-pay and book it before
exiting the arrival hall**. Once you exit, you cannot enter back in.

Pre-paid taxi counter is just after the Customs check, on the right side. Give
them the destination **Four Points Hotel, Info Park**.

Some points to remember while booking the prepaid taxi:

- **Payment can be done in card/cash**
- **Taxi should be taken 5-6 minutes after the payment is done**. So book the
taxi only when you are ready to leave. If you are sharing with someone, wait
for the last person to come before booking.
- **Taxi will collect you at prepaid taxi pillar A9** on the left side of the
exit. You can find your taxi number on the receipt.

### Uber / OLA
App-based taxi services like Uber and OLA are also considerable options. You can
also book directly from the websites of [Uber](https://www.uber.com) and
[OLA](https://www.olacabs.com) if you don't want to install their proprietary
apps.

### Make My Trip / Goibebo
[Make My Trip](https://www.makemytrip.com/car-rental/cochin-airport-taxi.html) and [Goibibo](https://www.goibibo.com/cars/cabs-from-kochi-airport) have options
to schedule pick up or drop at the airport. Choose "Four Points by Sheraton
Kochi Infopark" as destination. They offer free cancellation till 6 hours of
departure.

### Public transport
**Not recommended**
The venue is 28 km from the venue and a bit far from the regular public
transport routes. So the public transport options will include multiple
changeovers.

Aluva is the nearest bus stand. There may be buses from the airport to Aluva.
From Aluva, there are infrequent buses to Infopark. On working days, buses can
be really crowded.

[Bus schedule](https://aanavandi.com/) **(Please note: this schedule is just indicative, may not be perfect)**

Other options that shows public transport option for navigation.

- Google Maps (maps.google.com)
- OSMAnd app for android (Kochi metro only)
